# KlmTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.10.
This repository contains the code for **KLM Front-End test**, based on Angular framework. Dependencies include but not are limited to:

- [Angular][angular] ([Typescript][typescript]).


## Installation
`git` and `node` have _step-by-step_ documentation which allows you to successfully install these software on your machine:

- [Git installation process][git-installation],
- [node installation process][node-installation].

`typescript` and `angular-cli` must be installed globaly.

```bash
$ npm install -g typescript
$ npm install -g @angular/cli
```
Now that your system is ready, you can clone the project repository from [Github][github]:

```bash
git clone git clone https://SimonasRugevicius@bitbucket.org/SimonasRugevicius/klm-test.git
```

Install `node` dependencies inside your project folder:

```bash
$ cd /path/to/project
$ npm install
```
## Development server

In `api.service.ts` file on line 10 you must enter your own `api-key`, otherwise the application will not work properly.
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.