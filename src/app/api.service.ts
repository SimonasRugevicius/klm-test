import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  apiKey = '' // insert your api key

  constructor(private http: HttpClient) { }

  getData() {

    return this.getApiData().then(val => {

      if (val.error) {
        return val;
      } else {

        // Cherrypicking what I need, should use destructuring


        // const a = val.operationalFlights.map(({ route, flightNumber, airline, flightStatusPublicLangTransl, flightLegs }) => {
        //   return {
        //     route,
        //     flightNumber,
        //     airline,
        //     flightStatusPublicLangTransl,
        //     ...flightLegs.map(({ departureInformation: { times: { scheduled }, airport: { city: { name }, places: { checkInZone, gateNumber } } } }) => {
        //       return {
        //         departureTime: scheduled,
        //         departureCity: name,
        //         // checkInZone : places === undefined ? [''] : checkInZone,
        //         // gate : places === undefined ? [''] : gateNumber,
        //       }
        //     })[0]
        //   }

        // })

        return val.operationalFlights.map(el => {
          return {
            route: el.route,
            departureTime: el.flightLegs[0].departureInformation.times.scheduled,
            departureCity: el.flightLegs[0].departureInformation.airport.city.name,
            arivalTime: el.flightLegs[0].arrivalInformation.times.scheduled,
            arivalCity: el.flightLegs[0].arrivalInformation.airport.city.name,
            flightNumber: el.flightNumber,
            airline: el.airline,
            status: el.flightStatusPublicLangTransl,
            checkInDesk: el.flightLegs[0].departureInformation.airport.places === undefined ? [''] : el.flightLegs[0].departureInformation.airport.places.checkInZone,
            gate: el.flightLegs[0].departureInformation.airport.places === undefined ? [''] : el.flightLegs[0].departureInformation.airport.places.gateNumber

          }

        })

      }

    })

  }



  async getApiData() {
    try {

      const httpOptions = {
        headers: new HttpHeaders({
          'api-key': this.apiKey,
          'Accept': '*/*'
        })
      };



      const res = await this.http.get('https://api.airfranceklm.com/opendata/flightstatus/', httpOptions).toPromise();
      return res;

    } catch (e) {
      return e;
    }
  }

}
