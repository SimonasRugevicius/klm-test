import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';


@Component({
  selector: 'app-flight-data',
  templateUrl: './flight-data.component.html',
  styleUrls: ['./flight-data.component.css']
})


export class FlightDataComponent implements OnInit {


  displayedColumns = ['time', 'destination', 'status', 'details'];
  dataSource;
  data;
  error;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator

  constructor(private api: ApiService, public dialog: MatDialog) {

    api.getData().then(val => {
      if(val.error){
        this.error = val.error
      } else {
        this.error = false;
        this.makeTable(val)

      }
    });

  }


  ngOnInit() {
  }

  makeTable(val) {
    this.dataSource = new MatTableDataSource(val);
    this.dataSource.paginator = this.paginator;

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  openDialog(data): void {
    this.dialog.open(DialogComponent, {
      width: '300px',
      data
    });

  }

}
